﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroExtinguidores
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void registroClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmgestionCliente fgc = new FrmgestionCliente();
            fgc.MdiParent = this;
            fgc.DsCliente = dsValor;
            fgc.Show();
        }

        private void registroExtinguidoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProducto fgp = new FrmGestionProducto();
            fgp.MdiParent = this;
            fgp.DsProducto = dsValor;
            fgp.Show();

        }
    }
}
