﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroExtinguidores
{
 
    public partial class FrmgestionCliente : Form
    {
        private DataSet dsCliente;
        private BindingSource bsCliente;

        public DataSet DsCliente
        {
            get
            {
                return dsCliente;
            }

            set
            {
                dsCliente = value;
            }
        }

        public BindingSource BsCliente
        {
            get
            {
                return bsCliente;
            }

            set
            {
                bsCliente = value;
            }
        }

        

        public FrmgestionCliente()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FrmCliente fd = new FrmCliente();
            fd.DsFormularioc = dsCliente;
            fd.TblCliente = DsCliente.Tables["Cliente"];
            fd.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;
            FrmCliente fd = new FrmCliente();
            fd.TblCliente = DsCliente.Tables["Cliente"];
            fd.DsFormularioc = dsCliente;
            fd.DrCliente = drow;
            fd.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DsCliente.Tables["Cliente"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
