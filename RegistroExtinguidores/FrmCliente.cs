﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroExtinguidores
{
    public partial class FrmCliente : Form
    {
        private DataSet dsFormularioc;
        private BindingSource bsFormularioc;
        private DataRow drCliente;
        private DataTable tblCliente
            ;

        public DataSet DsFormularioc
        {
            get
            {
                return dsFormularioc;
            }

            set
            {
                dsFormularioc = value;
            }
        }

        public BindingSource BsFormularioc
        {
            get
            {
                return bsFormularioc;
            }

            set
            {
                bsFormularioc = value;
            }
        }

        public DataRow DrCliente
        {
            set
            {
                drCliente = value;
                textBox1.Text = drCliente["Cedula"].ToString();
                textBox2.Text = drCliente["Nombre"].ToString();
                textBox3.Text = drCliente["Apellido"].ToString();
                textBox4.Text = drCliente["Celular"].ToString();
                textBox5.Text = drCliente["Correo"].ToString();
                textBox6.Text= drCliente["Direccion"].ToString();

            }
        }

        public DataTable TblCliente
        {
            get
            {
                return tblCliente;
            }

            set
            {
                tblCliente = value;
            }
        }

        public FrmCliente()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string cedula, nombre, apellido, celular, correo, direccion;
            cedula = textBox1.Text;
            nombre = textBox2.Text;
            apellido = textBox3.Text;
            celular = textBox4.Text;
            correo = textBox5.Text;
            direccion = textBox6.Text;

            if (drCliente != null)
            {
                DataRow drnew = TblCliente.NewRow();
                int index = TblCliente.Rows.IndexOf(drCliente);
                drnew["Id"] = drCliente["Id"];
                drnew["Cedula"] = cedula;
                drnew["Nombres"] = nombre;
                drnew["Apellido"] = apellido;
                drnew["Celular"] = celular;
                drnew["Correo"] = correo;
                drnew["Direccion"] = direccion;


                TblCliente.Rows.RemoveAt(index);
                TblCliente.Rows.InsertAt(drnew, index);
            }
            else
            {
                TblCliente.Rows.Add(TblCliente.Rows.Count + 1,cedula, nombre, apellido, celular, correo, direccion);
            }
            Dispose();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsFormularioc.DataSource = DsFormularioc;
            bsFormularioc.DataMember = DsFormularioc.Tables["Cliente"].TableName;
        }
    }
}
