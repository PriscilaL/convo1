﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroExtinguidores.Clases
{
    class Producto
    {
        private int id;
        private string categoría;
        private string tipo;
        private string marca;
        private string capacidad;
        private string medida;
        private string designacion;
        private string recarga;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Categoría
        {
            get
            {
                return categoría;
            }

            set
            {
                categoría = value;
            }
        }

        public string Tipo
        {
            get
            {
                return tipo;
            }

            set
            {
                tipo = value;
            }
        }

        public string Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        public string Capacidad
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        public string Medida
        {
            get
            {
                return medida;
            }

            set
            {
                medida = value;
            }
        }

        public string Designacion
        {
            get
            {
                return designacion;
            }

            set
            {
                designacion = value;
            }
        }

        public string Recarga
        {
            get
            {
                return recarga;
            }

            set
            {
                recarga = value;
            }
        }

        public Producto(int id, string categoría, string tipo, string marca, string capacidad, string medida, string designacion, string recarga)
        {
            this.id = id;
            this.categoría = categoría;
            this.tipo = tipo;
            this.marca = marca;
            this.capacidad = capacidad;
            this.medida = medida;
            this.designacion = designacion;
            this.recarga = recarga;
        }
    }
}
