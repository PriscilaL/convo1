﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroExtinguidores.Clases
{
    class Cliente
    {
       private int id;
       private string cedula;
       private string nombres;
       private string apellidos;
       private string celular;
       private string correo;
       private string direccion;
       private Municipio muni;

        public Cliente(int id, string cedula, string nombres, string apellidos, string celular, string correo, string direccion, Municipio muni)
        {
            this.id = id;
            this.cedula = cedula;
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.celular = celular;
            this.correo = correo;
            this.direccion = direccion;
            this.muni = muni;
        }

        internal Municipio muni
        {
            get
            {
                return muni;
            }

            set
            {
                muni = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public string Nombres
        {
            get
            {
                return nombres;
            }

            set
            {
                nombres = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public string Celular
        {
            get
            {
                return celular;
            }

            set
            {
                celular = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public enum Municipio
        {
            Managua, Masaya,Granada,Chinandega, Nueva_segovia,Leon
        }
    }
}
