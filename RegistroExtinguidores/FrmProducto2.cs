﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroExtinguidores
{
    public partial class FrmProducto2 : Form
    {
        private DataSet dsFormulariop;
        private BindingSource bsFormulariop;
        private DataRow drProducto;
        private DataTable tblProducto;

        public DataSet DsFormulariop
        {
            get
            {
                return dsFormulariop;
            }

            set
            {
                dsFormulariop = value;
            }
        }

        public BindingSource BsFormulariop
        {
            get
            {
                return bsFormulariop;
            }

            set
            {
                bsFormulariop = value;
            }
        }

        public DataRow DrProducto
        {
            set
            {
                drProducto = value;
                textBox2.Text = drProducto["Categoria"].ToString();
                textBox3.Text = drProducto["Tipo"].ToString();
                textBox4.Text = drProducto["Marca"].ToString();
                textBox5.Text = drProducto["Capacidad"].ToString();
                textBox6.Text = drProducto["Medida"].ToString();
                textBox7.Text = drProducto["Designacion"].ToString();
                textBox8.Text = drProducto["Recarga"].ToString();
            }
        }

        public DataTable TblProducto
        {
            get
            {
                return tblProducto;
            }

            set
            {
                tblProducto = value;
            }
        }

        public FrmProducto2()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string categoria,tipo,marca,capacidad,medida,designacion,recarga;
            categoria = textBox2.Text;
            tipo = textBox3.Text;
            marca = textBox4.Text;
            capacidad = textBox5.Text;
            medida = textBox6.Text;
            designacion = textBox7.Text;
            recarga = textBox8.Text;

            if (drProducto != null)
            {
                DataRow drnew = TblProducto.NewRow();
                int index = TblProducto.Rows.IndexOf(drProducto);
                drnew["Id"] = drProducto["Id"];
                drnew["Categoria"] = categoria;
                drnew["Tipo"] = tipo;
                drnew["Marca"] = marca;
                drnew["Capacidad"] = capacidad;
                drnew["Medida"] = medida;
                drnew["Designacion"] = designacion;
                drnew["recarga"] = recarga;

                TblProducto.Rows.RemoveAt(index);
                TblProducto.Rows.InsertAt(drnew, index);
            }
            else
            {
                TblProducto.Rows.Add(TblProducto.Rows.Count + 1,categoria,tipo,marca,capacidad,medida,designacion,recarga);
            }
            Dispose();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void FrmProducto2_Load(object sender, EventArgs e)
        {
            bsFormulariop.DataSource = DsFormulariop;
            bsFormulariop.DataMember = DsFormulariop.Tables["Producto"].TableName;
        }
    }
}
