﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroExtinguidores
{
    public partial class FrmGestionProducto : Form
    {
        private DataSet dsProducto;
        private BindingSource bsProducto;

        public DataSet DsProducto
        {
            get
            {
                return dsProducto;
            }

            set
            {
                dsProducto = value;
            }
        }

        public BindingSource BsProducto
        {
            get
            {
                return bsProducto;
            }

            set
            {
                bsProducto = value;
            }
        }

        public FrmGestionProducto()
        {
            InitializeComponent();

            bsProducto = new BindingSource();
        }

        private void FrmGestionProducto_Load(object sender, EventArgs e)
        {

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmProducto2 fp = new FrmProducto2();
            fp.DsFormulariop = dsProducto;
            fp.TblProducto = DsProducto.Tables["Producto"];
            fp.ShowDialog();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;
            FrmProducto2 fd = new FrmProducto2();
            fd.TblProducto = DsProducto.Tables["Producto"];
            fd.DsFormulariop = dsProducto;
            fd.DrProducto = drow;
            fd.ShowDialog();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DsProducto.Tables["Producto"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
